# Set Up Your Golang GraphQL Project and gqlgen

## In this step, we will be setting up our Golang project and installing the gqlgen Golang GraphQL library.

Create a directory for your project. In your working directory, open your terminal and initialize it as a Go Module.
> go mod init github.com/<user-name>/go-graphql

Execute the following command to install the gqlgen library.
> go install github.com/99designs/gqlgen@latest
Bash

Create a file called tools.go at the root of your project, where you will list all your dependencies and paste the following code snippet.
```
// tools.go

package tools
import _ "github.com/99designs/gqlgen"
```

Open your terminal and execute the tidy command to add all missing dependencies.
> go mod tidy

Create the Golang GraphQL project structure using the init command.
> go run github.com/99designs/gqlgen init

## Define Your GraphQL Schema

refer graph/schema.graphqls

## Generate Your GraphQL Resolvers

Once you make changes to schema.graphqls generate responses for a GraphQL query.

Execute the following command to use the gqlgen code generation feature to generate resolvers based on your defined schema.
> go run github.com/99designs/gqlgen generate

## Connecting to data base

Refer storage/postgres.go for the connection details and give the respective connection url in the .env variables.

> DB_URL=postgresql://postgres:yathi3188@localhost:5432/go_graphql?sslmode=disable

## Implement Generated Resolvers

Implemet the logic for the generated resolvers and run the code.

## Test Your Golang GraphQL API

Start your Golang server 
> go run ./server.go

Head over to your browser, then navigate to http://localhost:8080 to access the in-built GraphQL playground.