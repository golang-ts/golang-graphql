package service

import (
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	ID       string `json:"id"`
	Username string `json:"username"`
	Password string `json:"password"`
}

type WrongUsernameOrPasswordError struct{}

func (m *WrongUsernameOrPasswordError) Error() string {
	return "wrong username or password"
}

// HashPassword hashes given password
func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

// CheckPassword hash compares raw password with it's hashed values
func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

// func GetUserIdByUsername(username string) (int, error) {
// 	statement, err := database.Db.Prepare("select ID from Users WHERE Username = ?")
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	row := statement.QueryRow(username)

// 	var Id int
// 	err = row.Scan(&Id)
// 	if err != nil {
// 		if err != sql.ErrNoRows {
// 			log.Print(err)
// 		}
// 		return 0, err
// 	}

// 	return Id, nil
// }
