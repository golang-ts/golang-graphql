package service

type Employee struct {
	ID    string `json:"id"`
	Email string `json:"email"`
	Role  string `json:"role"`
	Skill string `json:"skill"`
	User  *User  `json:"user"`
}
