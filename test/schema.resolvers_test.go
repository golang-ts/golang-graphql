package test

import (
	"context"
	"testing"

	"github.com/go-pg/pg"
	"github.com/stretchr/testify/require"
	"github.com/yathindrarao/go-graphql/graph/model"
)

type TestResolver struct {
	DB *pg.DB
}

// type mutationResolver struct{ *Resolver }

var test *graph.mutationResolver

func TestCreateUser(t *testing.T) {
	arg := model.NewUser{
		ID:       "1",
		Username: "yathindra",
		Password: "123",
	}

	user, err := test.CreateUser(context.Background(), arg)
	require.NoError(t, err)
	require.NotEmpty(t, user)

}

func TestCreateEmployee(t *testing.T) {
	user := model.User{
		ID:       "1",
		Username: "yathindra",
		Password: "123",
	}

	arg := model.CreateEmployeeInput{
		ID:    "1",
		Email: "Yathindra@gmail.com",
		Role:  "ASDE2",
		Skill: "Golang",
		Name:  user.Username,
	}

	employee, err := test.CreateEmployee(context.Background(), arg.ID, arg)
	require.NoError(t, err)
	require.NotEmpty(t, employee)

	require.Equal(t, arg.ID, employee.ID)
	require.Equal(t, arg.Email, employee.Email)
	require.Equal(t, arg.Role, employee.Role)
	require.Equal(t, arg.Skill, employee.Skill)
	require.Equal(t, arg.Name, employee.Name)

}
